unit Postagem;

interface

uses
  SysUtils, Classes, HTTPApp;

type
  TWebModule2 = class(TWebModule)
    procedure WebModule2WebActionItem1Action(Sender: TObject;
      Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  WebModule2: TWebModule2;

implementation

uses WebReq;

{$R *.dfm}

procedure TWebModule2.WebModule2WebActionItem1Action(Sender: TObject;
  Request: TWebRequest; Response: TWebResponse; var Handled: Boolean);
begin
  Response.Content:= 'Conte�do postado:<br>';
  Response.Content:= Response.Content + Request.Content + '</p>';

  Response.Content:= Response.Content + 'Conte�do Get:<br>';
  Response.Content:= Response.Content + Request.QueryFields.CommaText;

end;

initialization
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := TWebModule2;

end.
