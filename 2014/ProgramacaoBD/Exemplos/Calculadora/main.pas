unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TfrmCalculadora }

  TfrmCalculadora = class(TForm)
    btnSoma: TButton;
    btnSubtracao: TButton;
    btnMulti: TButton;
    btnDiv: TButton;
    edtSoma1: TEdit;
    edtSoma2: TEdit;
    edtSubtracao1: TEdit;
    edtSubtracao2: TEdit;
    edtMulti1: TEdit;
    edtMulti2: TEdit;
    edtDiv1: TEdit;
    edtDiv2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    lblResSoma: TLabel;
    lblResSubtracao: TLabel;
    Label5: TLabel;
    lblResMulti: TLabel;
    Label7: TLabel;
    lblResDiv: TLabel;
    Label9: TLabel;
    memHistorico: TMemo;
    procedure btnDivClick(Sender: TObject);
    procedure btnMultiClick(Sender: TObject);
    procedure btnSomaClick(Sender: TObject);
    procedure btnSubtracaoClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmCalculadora: TfrmCalculadora;

implementation

{$R *.lfm}

{ TfrmCalculadora }

procedure TfrmCalculadora.btnSomaClick(Sender: TObject);
begin
  lblResSoma.Caption:= IntToStr(StrToInt(edtSoma1.Text) + StrToInt(edtSoma2.Text));
  memHistorico.lines.Add(edtSoma1.Text + ' + ' +  edtSoma2.Text + ' = ' + lblResSoma.Caption);
end;

procedure TfrmCalculadora.btnMultiClick(Sender: TObject);
begin
  lblResMulti.Caption:= IntToStr(StrToInt(edtMulti1.Text) * StrToInt(edtMulti2.Text));
  memHistorico.lines.Add(edtMulti1.Text + ' X ' +  edtMulti2.Text + ' = ' + lblResMulti.Caption);
end;

procedure TfrmCalculadora.btnDivClick(Sender: TObject);
begin
  lblResDiv.Caption:= IntToStr(StrToInt(edtDiv1.Text) div StrToInt(edtDiv2.Text));
  memHistorico.lines.Add(edtDiv1.Text + ' / ' +  edtDiv2.Text + ' = ' + lblResDiv.Caption);
end;

procedure TfrmCalculadora.btnSubtracaoClick(Sender: TObject);
begin
  lblResSubtracao.Caption:= IntToStr(StrToInt(edtSubtracao1.Text) - StrToInt(edtSubtracao2.Text));
  memHistorico.lines.Add(edtSubtracao1.Text + ' - ' +  edtSubtracao2.Text + ' = ' + lblResSubtracao.Caption);
end;

end.

