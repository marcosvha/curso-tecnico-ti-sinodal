unit RelatoriosForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql55conn, sqldb, db, FileUtil, LR_Class, LR_DBSet,
  Forms, Controls, Graphics, Dialogs;

type

  { TfrmRelatorios }

  TfrmRelatorios = class(TForm)
    conClassicModels: TMySQL55Connection;
    dsVendas: TDataSource;
    dsVendasDetalhes: TDataSource;
    rdsVendas: TfrDBDataSet;
    repVendas: TfrReport;
    qryVendas: TSQLQuery;
    qryVendasDetalhes: TSQLQuery;
    rdsVendasDetalhes: TfrDBDataSet;
    traClassicModels: TSQLTransaction;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmRelatorios: TfrmRelatorios;

implementation

{$R *.lfm}

end.

