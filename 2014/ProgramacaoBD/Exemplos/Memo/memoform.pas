unit MemoForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnAdicionar: TButton;
    btnLimpar: TButton;
    btnAdiconarLinha: TButton;
    cbxSomenteLeitura: TCheckBox;
    edtTexto: TEdit;
    GroupBox1: TGroupBox;
    memTexto: TMemo;
    radEsquerda: TRadioButton;
    radDireita: TRadioButton;
    radCentro: TRadioButton;
    procedure btnAdicionarClick(Sender: TObject);
    procedure btnAdiconarLinhaClick(Sender: TObject);
    procedure btnLimparClick(Sender: TObject);
    procedure cbxSomenteLeituraClick(Sender: TObject);
    procedure radCentroClick(Sender: TObject);
    procedure radDireitaClick(Sender: TObject);
    procedure radEsquerdaClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnAdicionarClick(Sender: TObject);
begin
  memTexto.Text:= Trim(memTexto.Text + ' ' + edtTexto.Text);
end;

procedure TForm1.btnAdiconarLinhaClick(Sender: TObject);
begin
  memTexto.Lines.Add(edtTexto.text);
end;

procedure TForm1.btnLimparClick(Sender: TObject);
begin
  edtTexto.Clear();
  memTexto.Lines.Clear();
end;

procedure TForm1.cbxSomenteLeituraClick(Sender: TObject);
begin
  memTexto.ReadOnly:= cbxSomenteLeitura.Checked;
end;

procedure TForm1.radCentroClick(Sender: TObject);
begin
  memTexto.Alignment:= taCenter;
end;

procedure TForm1.radDireitaClick(Sender: TObject);
begin
  memTexto.Alignment:= taRightJustify;
end;

procedure TForm1.radEsquerdaClick(Sender: TObject);
begin
  memTexto.Alignment:= taLeftJustify;
end;

end.

