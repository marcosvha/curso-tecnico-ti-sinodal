unit VendasForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql55conn, sqldb, db, FileUtil, Forms, Controls,
  Graphics, Dialogs, DBGrids, StdCtrls, DbCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnExcluir: TButton;
    btnGravar: TButton;
    btnIncluir: TButton;
    btnAlterar: TButton;
    DBLookupComboBox1: TDBLookupComboBox;
    dsItens: TDataSource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    dsVenda: TDataSource;
    dsVendaItens: TDataSource;
    edtQuantidade: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    MySQL55Connection1: TMySQL55Connection;
    qryVenda: TSQLQuery;
    qryVendaItens: TSQLQuery;
    qryItens: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    procedure btnGravarClick(Sender: TObject);
    procedure btnIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnGravarClick(Sender: TObject);
begin
  qryVendaItens.FieldByName('invoiceid').AsInteger:= qryVenda.FieldByName('invoiceid').AsInteger;
  qryVendaItens.FieldByName('itemid').AsInteger:= qryItens.FieldByName('itemid').AsInteger;
  qryVendaItens.FieldByName('invoiceitemcount').AsInteger:= StrToInt(edtQuantidade.Text);
  qryVendaItens.FieldByName('invoiceitemcost').AsFloat:= qryItens.FieldByName('itemcostvalue').AsFloat *
                                                  StrToInt(edtQuantidade.Text);
  qryVendaItens.FieldByName('invoiceitemvalue').AsFloat:= qryItens.FieldByName('itemvalue').AsFloat *
                                                   StrToInt(edtQuantidade.Text);
  qryVendaItens.FieldByName('invoiceitemtype').AsInteger:= 0; // Produto
  qryVendaItens.Post;
end;

procedure TForm1.btnIncluirClick(Sender: TObject);
begin
  qryVendaItens.Insert;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  qryItens.Open;
end;

end.

