unit ContatoClass;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils; 

type

    { TContato }

    TContato = class
    protected
      FID: integer;
      FNome: string;
      FCPF: string;
      FRG: string;
      FSexo: Char;
      FDtNasc: TDateTime;
      FObs: string;
    public
      function getID(): integer;
      function getNome(): string;
      function getCPF(): string;
      function getRG(): string;
      function getSexo(): Char;
      function getDtNasc(): TDateTime;
      function getObs(): string;
      procedure setID(ID: integer);
      procedure setNome(Nome: string);
      procedure setCPF(CPF: string);
      procedure setRG(RG: string);
      procedure setSexo(Sexo: Char);
      procedure setDtNasc(DtNasc: TDateTime);
      procedure setObs(Obs: string);
    end;


implementation

{ TContato }

function TContato.getID(): integer;
begin
  Result:= FID;
end;

function TContato.getNome(): string;
begin
  Result:= Self.FNome;
end;

function TContato.getCPF(): string;
begin
  Result:= FCPF;
end;

function TContato.getRG(): string;
begin
  Result:= FRG;
end;

function TContato.getSexo(): Char;
begin
  Result:= FSexo;
end;

function TContato.getDtNasc(): TDateTime;
begin
  Result:= FDtNasc;
end;

function TContato.getObs(): string;
begin
  Result:= FObs;
end;

procedure TContato.setID(ID: integer);
begin
  FID:= ID;

end;

procedure TContato.setNome(Nome: string);
begin
  FNome:= Nome;
end;

procedure TContato.setCPF(CPF: string);
begin
  FCPF:= CPF;
end;

procedure TContato.setRG(RG: string);
begin
  FRG:= RG;
end;

procedure TContato.setSexo(Sexo: Char);
begin
  FSexo:= Sexo;
end;

procedure TContato.setDtNasc(DtNasc: TDateTime);
begin
  FDtNasc:= DtNasc;
end;

procedure TContato.setObs(Obs: string);
begin
  FObs:= Obs;
end;

end.

