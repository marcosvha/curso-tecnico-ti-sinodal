unit ContatosDM;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql50conn, sqldb, FileUtil, LResources, Forms, Controls,
  Dialogs, ContatoClass, DB;

type

  { TdmContatos }

  TdmContatos = class(TDataModule)
    conContatos: TMySQL50Connection;
    traContatos: TSQLTransaction;
  private
    { private declarations }
  public
    { public declarations }
    function Grava(oContato: TContato): boolean;
    function Carrega(oContato: TContato): TContato;
    function CarregaLista(oContato: TContato): TDataSet;
  end; 

var
  dmContatos: TdmContatos;

implementation

{ TdmContatos }

function TdmContatos.Grava(oContato: TContato): boolean;
var
   qryTemp: TSqlQuery;
begin
   qryTemp:= TSqlQuery.Create(self);
   qryTemp.DataBase:= conContatos;
   qryTemp.SQL.Add('INSERT INTO CONTATO (nome, cpf, rg, dtnasc, obs) ');
   qryTemp.SQL.Add('VALUES (:nome, :cpf, :rg, :dtnasc, :obs) ');

   qryTemp.Params.ParamByName('nome').AsString:= oContato.getNome();
   qryTemp.Params.ParamByName('cpf').AsString:= oContato.getCPF();
   qryTemp.Params.ParamByName('rg').AsString:= oContato.getRG();
   qryTemp.Params.ParamByName('dtnasc').AsDateTime:= oContato.getDtNasc();
   qryTemp.Params.ParamByName('obs').AsString:= oContato.getObs();

   try
     qryTemp.ExecSQL();
     Result:= True;
   except
     // Se ocorreu algum erro no INSERT
     Result:= False;
   end;

   qryTemp.Free();
end;

function TdmContatos.Carrega(oContato: TContato): TContato;
var
   qryTemp: TSqlQuery;
begin
   qryTemp:= TSqlQuery.Create(self);
   qryTemp.DataBase:= conContatos;
   qryTemp.SQL.Add('SELECT * FROM contato ');
   qryTemp.SQL.Add('WHERE 1=1 ');

   if oContato.getId() > 0 then
   begin
      qryTemp.SQL.Add('and id=' + IntToStr(oContato.getID()));
   end;
   if oContato.getCPF() <> '' then
   begin
      qryTemp.SQL.Add('and cpf = "' + oContato.getCPF() + '" ');
   end;
   if oContato.getRG() <> '' then
   begin
      qryTemp.SQL.Add('and rg = "' + oContato.getRG() + '" ');
   end;

   if not conContatos.Connected then
     conContatos.Open();

   qryTemp.Open();

   Result:= TContato.Create();
   Result.setID(qryTemp.FieldByName('id').AsInteger);
   Result.setNome(qryTemp.FieldByName('nome').AsString);
   Result.setCPF(qryTemp.FieldByName('cpf').AsString);
   Result.setRG(qryTemp.FieldByName('rg').AsString);
   Result.setDtNasc(qryTemp.FieldByName('dtnasc').AsDateTime);
   Result.setObs(qryTemp.FieldByName('obs').AsString);
end;

function TdmContatos.CarregaLista(oContato: TContato): TDataSet;
var
   qryTemp: TSqlQuery;
begin
   qryTemp:= TSqlQuery.Create(self);
   qryTemp.DataBase:= conContatos;
   qryTemp.SQL.Add('SELECT * FROM contato ');
   qryTemp.SQL.Add('WHERE 1=1 ');

   if oContato.getNome() <> '' then
   begin
      qryTemp.SQL.Add('and nome like "%' + oContato.getNome() + '%" ');
   end;
   if oContato.getCPF() <> '' then
   begin
      qryTemp.SQL.Add('and cpf = "' + oContato.getCPF() + '" ');
   end;
   if oContato.getRG() <> '' then
   begin
      qryTemp.SQL.Add('and rg = "' + oContato.getRG() + '" ');
   end;

   qryTemp.SQL.Add('order by nome ');

   if not conContatos.Connected then
     conContatos.Open();

   qryTemp.Open();

   Result:= qryTemp;
end;

initialization
  {$I ContatosDM.lrs}

end.

