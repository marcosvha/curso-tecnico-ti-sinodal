unit utrymysql;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, ComCtrls, mysql4conn, sqldb;

type

  { TFormTryMySQL }

  TFormTryMySQL = class(TForm)
    MySQLConnection1: TMySQLConnection;
    OpenQueryButton: TButton;
    FieldLabel: TLabel;
    FieldListBox: TListBox;
    SQLQuery1: TSQLQuery;
    SQLTransaction1: TSQLTransaction;
    TableComboBox: TComboBox;
    DatabaseComboBox: TComboBox;
    TableLabel: TLabel;
    SelectDBButton: TButton;
    LabelDatabase: TLabel;
    ConnectButton: TButton;
    ExitButton: TButton;
    CommandEdit: TEdit;
    HostEdit: TEdit;
    CommandLabel: TLabel;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    UserEdit: TEdit;
    PasswdEdit: TEdit;
    HostLabel: TLabel;
    UserLabel: TLabel;
    PasswdLabel: TLabel;
    procedure ConnectButtonClick(Sender: TObject);
    procedure DatabaseComboBoxChange(Sender: TObject);
    procedure ExitButtonClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure OpenQueryButtonClick(Sender: TObject);
    procedure SelectDBButtonClick(Sender: TObject);
    procedure TableComboBoxChange(Sender: TObject);
  private
    { private declarations }
    procedure ClearSettings;
    procedure CloseConnection(Sender: TObject);
    procedure ShowString(const S: String);
  public
    { public declarations }
  end; 

var
  FormTryMySQL: TFormTryMySQL;

implementation

uses
  UShowQuery;

{ TFormTryMySQL }

procedure TFormTryMySQL.ConnectButtonClick(Sender: TObject);
begin
  // Check if we have an active connection. If so, let's close it.
  if MySQLConnection1.Connected then begin
    SQLTransaction1.Active := False;
    MySQLConnection1.Close;
  end;
  // Set the connection parameters.
  MySQLConnection1.HostName := HostEdit.Text;
  MySQLConnection1.UserName := UserEdit.Text;
  MySQLConnection1.Password := PasswdEdit.Text;
  MySQLConnection1.DatabaseName := 'mysql'; // MySQL is allways there!
  ShowString('Opening a connection to server: ' + HostEdit.Text);
  MySQLConnection1.Open;
  // First lets get a list of available databases.
  if MySQLConnection1.Connected then begin
    ShowString('Connected to server: ' + HostEdit.Text);
    ShowString('Retreiving list of available databases.');
    SQLQuery1.SQL.Text := 'show databases';
    SQLQuery1.Open;
    while not SQLQuery1.EOF do begin
      DatabaseComboBox.Items.Add(SQLQuery1.Fields[0].AsString);
      SQLQuery1.Next;
    end;
    SQLQuery1.Close;
    ShowString('List of databases received!');
  end;
end;

procedure TFormTryMySQL.DatabaseComboBoxChange(Sender: TObject);
begin
  // Enable the SelectDBButton when there are database in the list
  // and there has been one selected.
  SelectDBButton.Enabled := (DatabaseComboBox.Items.Count > 0)
                                          and (DatabaseComboBox.ItemIndex > -1);
end;

procedure TFormTryMySQL.ExitButtonClick(Sender: TObject);
begin
  Close;
end;

procedure TFormTryMySQL.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
  // Make sure our connection is closed, when we close our application.
  if MySQLConnection1.Connected then CloseConnection(Sender);
  CanClose := True;
end;

procedure TFormTryMySQL.FormCreate(Sender: TObject);
begin
  ShortTimeFormat := 'hh:nn:ss.zzz';
end;

procedure TFormTryMySQL.OpenQueryButtonClick(Sender: TObject);
begin
  ShowQueryForm := TShowQueryForm.Create(nil);
  try
    ShowQueryForm.DataSource1.DataSet := SQLQuery1;
    SQLQuery1.SQL.Text := CommandEdit.Text;
    SQLQuery1.Open;
    ShowQueryForm.ShowModal;
  finally
    ShowQueryForm.Free;
    SQLQuery1.Close;
  end;
end;

procedure TFormTryMySQL.SelectDBButtonClick(Sender: TObject);
begin
  // A database has been selected so lets get the tables in it.
  CloseConnection(Sender);
  if DatabaseComboBox.ItemIndex <> -1 then begin
    with DatabaseComboBox do
      MySQLConnection1.DatabaseName := Items[ItemIndex];
    SQLQuery1.SQL.Text := 'show tables';
    ShowString('Retrieving list of tables');
    SQLQuery1.Open;
    while not SQLQuery1.EOF do begin
      TableComboBox.Items.Add(SQLQuery1.Fields[0].AsString);
      SQLQuery1.Next;
    end;
    SQLQuery1.Close;
    ShowString('List of tables received');
  end;
  OpenQueryButton.Enabled := True;
end;

procedure TFormTryMySQL.TableComboBoxChange(Sender: TObject);
begin
  FieldListBox.Clear;
  SQLQuery1.SQL.Text := 'show columns from ' + TableComboBox.Text;
  ShowString('Retrieving fields for table ' + TableComboBox.Text);
  SQLQuery1.Open;
  while not SQLQuery1.EOF do begin
    FieldListBox.Items.Add(SQLQuery1.Fields[0].AsString);
    SQLQuery1.Next;
  end;
  SQLQuery1.Close;
  ShowString('List of fields received');
end;

procedure TFormTryMySQL.ClearSettings;
begin
  DatabaseComboBox.Items.Clear;
  TableComboBox.Items.Clear;
  FieldListBox.Items.Clear;
  SelectDBButton.Enabled := False;
  OpenQueryButton.Enabled := False;
end;

procedure TFormTryMySQL.CloseConnection(Sender: TObject);
begin
  // The SQLTransaction1 gets activated automatically, but before we can close
  // the connection we have to set the SQLTransaction1.Active to false.
  SQLTransaction1.Active := False;
  MySQLConnection1.Close;
  // Information about the list of tables will propably needs refresh
  // and the fields list of course as well.
  TableComboBox.Items.Clear;
  FieldListBox.Items.Clear;
  // OpenQueryButton should be disabled anyhow.
  OpenQueryButton.Enabled := False;
  // If this is called from anything else then from SelectDBButtonClick
  // the SelectDBButton should be disabled as well.
  if not (Sender = SelectDBButton) then begin
    SelectDBButton.Enabled := False;
  end;
end;

procedure TFormTryMySQL.ShowString(const S: String);
begin
  Memo1.Lines.Add(TimeToStr(now) + ': ' + S);
end;

initialization
  {$I utrymysql.lrs}

end.

