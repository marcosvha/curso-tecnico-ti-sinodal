unit ushowquery;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, Buttons, DB, DBGrids;

type

  { TShowQueryForm }

  TShowQueryForm = class(TForm)
    CloseButton: TButton;
    Datasource1: TDatasource;
    dbGrid1: TdbGrid;
    Panel1: TPanel;
    procedure CloseButtonClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  ShowQueryForm: TShowQueryForm;

implementation

{ TShowQueryForm }

procedure TShowQueryForm.CloseButtonClick(Sender: TObject);
begin
  Close;
end;

initialization
  {$I ushowquery.lrs}

end.

