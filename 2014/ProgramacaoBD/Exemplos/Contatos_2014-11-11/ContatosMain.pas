unit ContatosMain; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql50conn, mysql55conn, sqldb, db, FileUtil, LResources,
  Forms, Controls, Graphics, Dialogs, DBGrids, DbCtrls, ComCtrls, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBNavigator2: TDBNavigator;
    dtsEndereco: TDatasource;
    grdTelefone: TDBGrid;
    DBNavigator1: TDBNavigator;
    Label7: TLabel;
    conProgBD: TMySQL55Connection;
    navTelefone: TDBNavigator;
    dtsEmail: TDatasource;
    DBRadioGroup1: TDBRadioGroup;
    dtsTelefone: TDatasource;
    edtNome: TDBEdit;
    edtNome1: TDBEdit;
    edtNome2: TDBEdit;
    edtNome3: TDBEdit;
    grdContato: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    navContato: TDBNavigator;
    dtsContato: TDatasource;
    pagContato: TPageControl;
    qryContato: TSQLQuery;
    qryContatocpf: TStringField;
    qryContatodtnasc: TDateField;
    qryContatoid: TLongintField;
    qryContatonome: TStringField;
    qryContatoobs: TBlobField;
    qryContatorg: TStringField;
    qryContatosexo: TStringField;
    qryEmail: TSQLQuery;
    qryEndereco: TSQLQuery;
    qryTelefone: TSQLQuery;
    qryEmailcontatoid: TLongintField;
    qryEmailemail: TStringField;
    qryEmailid: TLongintField;
    qryEmailpadrao: TStringField;
    qryEmailtipo: TLongintField;
    tabGrid: TTabSheet;
    tabDetalhe: TTabSheet;
    traProgBD: TSQLTransaction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure grdContatoTitleClick(Column: TColumn);
    procedure pagContatoChange(Sender: TObject);
    procedure qryContatoAfterPost(DataSet: TDataSet);
    procedure qryEmailAfterInsert(DataSet: TDataSet);
    procedure qryEmailAfterPost(DataSet: TDataSet);
    procedure qryEnderecoAfterPost(DataSet: TDataSet);
    procedure qryTelefoneAfterPost(DataSet: TDataSet);
  private
    { private declarations }
    procedure CarregaContatos(CampoOrdem: string);
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  conProgBD.Connected := true;
  CarregaContatos('nome');
  qryEmail.Open;
  qryTelefone.Open;
  qryEndereco.Open;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  pagContato.ActivePageIndex:= 0;
end;

procedure TForm1.grdContatoTitleClick(Column: TColumn);
begin
  if Column.Title.ImageIndex = 1 then
  begin
    CarregaContatos(Column.FieldName + ' DESC ');
    Column.Title.ImageIndex := 2
  end
  else
  begin
    CarregaContatos(Column.FieldName);
    Column.Title.ImageIndex := 1;
  end;
end;

procedure TForm1.pagContatoChange(Sender: TObject);
begin

end;

procedure TForm1.qryContatoAfterPost(DataSet: TDataSet);
begin
  qryContato.ApplyUpdates(0);
end;

procedure TForm1.qryEmailAfterInsert(DataSet: TDataSet);
var
   iCount: integer;
begin
  iCount:= DataSet.RecordCount;
  DataSet.FieldByName('contatoid').AsInteger:= qryContato.FieldByName('id').AsInteger;
  DataSet.FieldByName('id').AsInteger:= iCount + 1;
end;

procedure TForm1.qryEmailAfterPost(DataSet: TDataSet);
begin
  qryEmail.ApplyUpdates;
end;

procedure TForm1.qryEnderecoAfterPost(DataSet: TDataSet);
begin
  qryEndereco.ApplyUpdates;
end;

procedure TForm1.qryTelefoneAfterPost(DataSet: TDataSet);
begin
  qryTelefone.ApplyUpdates;
end;

procedure TForm1.CarregaContatos(CampoOrdem: string);
begin
  qryContato.Close;
  qryContato.SQL.Text := 'select * from contato order by ' + CampoOrdem;
  qryContato.Open;
end;

initialization
  {$I ContatosMain.lrs}

end.

