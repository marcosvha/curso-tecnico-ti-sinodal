unit ContatosMain; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql50conn, sqldb, db, FileUtil, LResources, Forms,
  Controls, Graphics, Dialogs, DBGrids, DbCtrls, ComCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    conProgBD: TMySQL50Connection;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    grdContato: TDBGrid;
    navContato: TDBNavigator;
    dtsContato: TDatasource;
    PageControl1: TPageControl;
    qryContato: TSQLQuery;
    qryContatocpf: TStringField;
    qryContatodtnasc: TDateField;
    qryContatoid: TLongintField;
    qryContatonome: TStringField;
    qryContatoobs: TBlobField;
    qryContatorg: TStringField;
    qryContatosexo: TStringField;
    pagContato: TTabSheet;
    TabSheet2: TTabSheet;
    traProgBD: TSQLTransaction;
    procedure FormCreate(Sender: TObject);
    procedure qryContatoAfterPost(DataSet: TDataSet);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  conProgBD.Connected := true;
  qryContato.Active := true;
end;

procedure TForm1.qryContatoAfterPost(DataSet: TDataSet);
begin
  qryContato.ApplyUpdates(0);
end;

initialization
  {$I ContatosMain.lrs}

end.

