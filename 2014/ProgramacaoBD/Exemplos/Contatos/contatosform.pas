unit ContatosForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldblib, sqldb, db, mysql55conn, FileUtil, Forms,
  Controls, Graphics, Dialogs, DBGrids, DbCtrls, ComCtrls, StdCtrls;

type

  { TfrmContato }

  TfrmContato = class(TForm)
    conProgBD: TMySQL55Connection;
    memObs: TDBMemo;
    radSexo: TDBRadioGroup;
    edtNome: TDBEdit;
    edtCPF: TDBEdit;
    edtRG: TDBEdit;
    edtDtNasc: TDBEdit;
    grdContato: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    llMySQL: TSQLDBLibraryLoader;
    navContato: TDBNavigator;
    dtsContato: TDatasource;
    pagContato: TPageControl;
    qryContato: TSQLQuery;
    qryContatocpf: TStringField;
    qryContatodtnasc: TDateField;
    qryContatoid: TAutoIncField;
    qryContatonome: TStringField;
    qryContatoobs: TMemoField;
    qryContatorg: TStringField;
    qryContatosexo: TStringField;
    tabGrid: TTabSheet;
    tabForm: TTabSheet;
    traProgBD: TSQLTransaction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryContatoAfterDelete(DataSet: TDataSet);
    procedure qryContatoAfterPost(DataSet: TDataSet);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  frmContato: TfrmContato;

implementation

{$R *.lfm}

{ TfrmContato }


procedure TfrmContato.qryContatoAfterPost(DataSet: TDataSet);
begin
  qryContato.ApplyUpdates(0);
end;


procedure TfrmContato.qryContatoAfterDelete(DataSet: TDataSet);
begin
  qryContato.ApplyUpdates(0);
end;

procedure TfrmContato.FormCreate(Sender: TObject);
begin
  llMySQL.Enabled := false;
  conProgBD.Connected:= true;
  qryContato.Active:= true;
end;

procedure TfrmContato.FormShow(Sender: TObject);
begin
  pagContato.ActivePageIndex:= 0;
end;

end.

