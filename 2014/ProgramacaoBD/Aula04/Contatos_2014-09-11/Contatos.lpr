program Contatos;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, ContatosMain, LResources, SQLDBLaz
  { you can add units after this };

{$IFDEF WINDOWS}{$R Contatos.rc}{$ENDIF}

begin
  {$I Contatos.lrs}
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.

