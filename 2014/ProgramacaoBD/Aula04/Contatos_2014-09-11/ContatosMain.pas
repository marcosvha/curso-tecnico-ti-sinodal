unit ContatosMain; 

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql50conn, sqldb, db, FileUtil, LResources, Forms,
  Controls, Graphics, Dialogs, DBGrids, DbCtrls, ComCtrls, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    conProgBD: TMySQL50Connection;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    dtsEmail: TDatasource;
    grdContato: TDBGrid;
    dtsContato: TDatasource;
    grdEmail: TDBGrid;
    Label1: TLabel;
    navContato: TDBNavigator;
    navContato2: TDBNavigator;
    PageControl1: TPageControl;
    qryContato: TSQLQuery;
    qryContatocpf: TStringField;
    qryContatodtnasc: TDateField;
    qryContatoid: TLongintField;
    qryContatonome: TStringField;
    qryContatoobs: TBlobField;
    qryContatorg: TStringField;
    qryContatosexo: TStringField;
    pagContato: TTabSheet;
    qryEmail: TSQLQuery;
    qryEmailcontatoid: TLongintField;
    qryEmailemail: TStringField;
    qryEmailid: TLongintField;
    qryEmailpadrao: TStringField;
    qryEmailtipo: TLongintField;
    TabSheet2: TTabSheet;
    traProgBD: TSQLTransaction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryContatoAfterPost(DataSet: TDataSet);
    procedure qryEmailAfterInsert(DataSet: TDataSet);
    procedure qryEmailAfterPost(DataSet: TDataSet);
    procedure TabSheet1Show(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  conProgBD.Connected := true;
  qryContato.Active := true;
  qryEmail.Active:= true;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
end;

procedure TForm1.qryContatoAfterPost(DataSet: TDataSet);
begin
  qryContato.ApplyUpdates(0);
end;

procedure TForm1.qryEmailAfterInsert(DataSet: TDataSet);
var
  iNumReg: integer;
begin
  iNumReg:= DataSet.RecordCount + 1;
  DataSet.FieldByName('contatoid').AsInteger := qryContato.FieldByName('id').AsInteger;
  DataSet.FieldByName('id').AsInteger := iNumReg;
end;

procedure TForm1.qryEmailAfterPost(DataSet: TDataSet);
begin
  qryEmail.ApplyUpdates(0);
end;

procedure TForm1.TabSheet1Show(Sender: TObject);
begin
end;

initialization
  {$I ContatosMain.lrs}

end.

