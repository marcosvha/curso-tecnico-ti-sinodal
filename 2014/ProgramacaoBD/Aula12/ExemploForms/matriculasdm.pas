unit MatriculasDM;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql56conn, mysql55conn, sqldb, db, FileUtil;

type

  { TdmMatriculas }

  TdmMatriculas = class(TDataModule)
    dsContatos: TDataSource;
    dsCursos: TDataSource;
    dsMatriculas: TDataSource;
    conMatriculas: TMySQL55Connection;
    qryContatos: TSQLQuery;
    qryCursos: TSQLQuery;
    qryMatriculas: TSQLQuery;
    qryMatriculascontatoid: TLongintField;
    qryMatriculascursoid: TLongintField;
    qryMatriculasdatafim: TDateField;
    qryMatriculasdatainicio: TDateField;
    StringField1: TStringField;
    traMatriculas: TSQLTransaction;
    procedure qryCursosAfterPost(DataSet: TDataSet);
    procedure qryMatriculasAfterPost(DataSet: TDataSet);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  dmMatriculas: TdmMatriculas;

implementation

{$R *.lfm}

{ TdmMatriculas }

procedure TdmMatriculas.qryCursosAfterPost(DataSet: TDataSet);
begin
  qryCursos.ApplyUpdates;
end;

procedure TdmMatriculas.qryMatriculasAfterPost(DataSet: TDataSet);
begin
  qryMatriculas.ApplyUpdates;
end;

end.

