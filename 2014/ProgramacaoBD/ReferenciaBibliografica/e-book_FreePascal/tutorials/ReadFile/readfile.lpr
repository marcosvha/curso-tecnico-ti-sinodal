program ReadContents;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils
  { you can add units after this };


{$IFDEF WINDOWS}{$R readfile.rc}{$ENDIF}
var
  FileName: string;
  F: file;
  Block: array [0 .. 1023] of Byte;
  i, NumRead: Integer;
begin
  Write('Input source file name: ');
  Readln(FileName);

  if FileExists(FileName) then
  begin
    AssignFile(F, FileName);

    FileMode:= 0;   // open for read only
    Reset(F, 1);

    while not Eof(F) do
    begin
      BlockRead(F, Block, SizeOf(Block), NumRead);
      // display contents in screen
      for i:= 0 to NumRead - 1 do
        Writeln(Block[i], ':', Chr(Block[i]));
    end;
    CloseFile(F);

  end
  else // File does not found
    Writeln('Source File does not exist');

  Write('press enter key to close..');
  Readln;
end.

