unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, DbCtrls, memds, db, DBGrids;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Datasource1: TDatasource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DBText1: TDBText;
    MemDataset1: TMemDataset;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  Form1: TForm1; 

implementation

uses Second;

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  MemDataset1.SaveToFile('table.dat');
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  MemDataset1.Open;
end;

initialization
  {$I main.lrs}

end.

