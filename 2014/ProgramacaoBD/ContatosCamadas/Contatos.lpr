program Contatos;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, ContatosCad, LResources, SQLDBLaz, ContatoClass, ContatosDM;

{$IFDEF WINDOWS}{$R Contatos.rc}{$ENDIF}

begin
  {$I Contatos.lrs}
  Application.Initialize;
  Application.CreateForm(TfrmContatos, frmContatos);
  Application.CreateForm(TdmContatos, dmContatos);
  Application.Run;
end.

