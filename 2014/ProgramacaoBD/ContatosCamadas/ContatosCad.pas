unit ContatosCad;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  Grids, StdCtrls, EditBtn, db, ContatoClass, ContatosDM;

type

  { TfrmContatos }

  TfrmContatos = class(TForm)
    btnAlterar: TButton;
    btnGravar: TButton;
    btnCancelar: TButton;
    btnPesquisar: TButton;
    btnIncluir: TButton;
    edtDtNasc: TDateEdit;
    edtCPF: TEdit;
    edtNome: TEdit;
    edtPesquisaRG: TEdit;
    edtPesquisaNome: TEdit;
    edtPesquisaCPF: TEdit;
    edtRG: TEdit;
    grdContatos: TStringGrid;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    memObs: TMemo;
    procedure btnGravarClick(Sender: TObject);
    procedure btnPesquisarClick(Sender: TObject);
    procedure grdContatosClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end; 

var
  frmContatos: TfrmContatos;

implementation

{ TfrmContatos }

procedure TfrmContatos.btnPesquisarClick(Sender: TObject);
var
  iLinha: integer;
  oContato: TContato;
  dsTemp: TDataSet;
begin
  oContato:= TContato.Create();
  oContato.setNome(edtPesquisaNome.Text);
  oContato.setCPF(edtPesquisaCPF.Text);
  oContato.setRG(edtPesquisaRG.Text);

  grdContatos.Clean;
  grdContatos.Cells[0, 0]:= 'ID';
  grdContatos.Cells[1, 0]:= 'Nome';
  grdContatos.Cells[2, 0]:= 'CPF';
  grdContatos.Cells[3, 0]:= 'RG';
  grdContatos.Cells[4, 0]:= 'Data Nasc.';
  grdContatos.Cells[5, 0]:= 'Obs.';

  dsTemp:= dmContatos.CarregaLista(oContato);
  grdContatos.RowCount:= dsTemp.RecordCount + 1;
  dsTemp.First;
  iLinha:= 1;
  while not dsTemp.EOF do
  begin
    grdContatos.Cells[0, iLinha]:= dsTemp.FieldByName('id').AsString;
    grdContatos.Cells[1, iLinha]:= dsTemp.FieldByName('nome').AsString;
    grdContatos.Cells[2, iLinha]:= dsTemp.FieldByName('cpf').AsString;
    grdContatos.Cells[3, iLinha]:= dsTemp.FieldByName('rg').AsString;
    grdContatos.Cells[4, iLinha]:= dsTemp.FieldByName('dtnasc').AsString;
    grdContatos.Cells[5, iLinha]:= dsTemp.FieldByName('obs').AsString;
    dsTemp.Next;
    iLinha:= iLinha + 1;
  end;

  dsTemp.Close;
  dsTemp.Free;
  oContato.Free;
end;

procedure TfrmContatos.grdContatosClick(Sender: TObject);
var
  oContato1, oContato2: TContato;
begin
  oContato1:= TContato.Create();
  oContato1.setID(StrToInt(grdContatos.Cells[0, grdContatos.Row]));

  oContato2:= dmContatos.Carrega(oContato1);
  edtNome.Text:= oContato2.getNome();
  edtCPF.Text:= oContato2.getCPF();
  edtRG.Text:= oContato2.getRG();
  edtDtNasc.Date:= oContato2.getDtNasc();
  memObs.Text:= oContato2.getObs();

end;

procedure TfrmContatos.btnGravarClick(Sender: TObject);
var
  oContato: TContato;
begin
  oContato:= TContato.Create();
  oContato.setNome(edtNome.Text);
  oContato.setCPF(edtCPF.Text);
  oContato.setRG(edtRG.Text);
  oContato.setDtNasc(edtDtNasc.Date);
  oContato.setObs(memObs.Text);

  if dmContatos.Grava(oContato) then
     ShowMessage('Contato gravado com sucesso')
  else
     ShowMessage('Erro ao gravar contato');

  oContato.Free();
end;

initialization
  {$I ContatosCad.lrs}

end.

