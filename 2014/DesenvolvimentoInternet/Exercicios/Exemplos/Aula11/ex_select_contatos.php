<html>
<head>
	<style>
	  th, td {
		border: solid 1px #000000;
		padding: 0;
		margin: 0;
		font-family: Tahoma;
	  }
	</style>
</head>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "csp_progbd";

// Cria um objeto de conex�o
$conn = new mysqli($servername, $username, $password, $dbname);
// Verifica se a conex�o foi bem sucedida
if ($conn->connect_error) {
    die("Erro de conex�o: " . $conn->connect_error);
} 

// Monta o select (query)
$sql = "SELECT * from contato order by nome";
// Executa a query e retorna um objeto (DataSet)
$result = $conn->query($sql);
?>
<table cellspacing="0">
	<th>ID</th>
	<th>Nome</th>
	<th>CPF</th>
	<th>RG</th>
<?php
// Se possui linhas de resultado
if ($result->num_rows > 0) {
    // L� uma linha e armazena em um objeto
    while($row = $result->fetch_assoc()) {
		echo "<tr>";
        echo "<td>" . $row["id"] . "</td>";
        echo "<td>" . $row["nome"] . "</td>";
        echo "<td>" . $row["cpf"] . "</td>";
        echo "<td>" . $row["rg"] . "</td>";
		echo "</tr>";
    }
	echo "</table>";
} else {
    echo "Sem resultados";
}

//Fecha a conex�o e libera mem�ria
$conn->close();
?>