package calculalucroproduto;

import java.util.Scanner;

public class CalculaLucroProduto {

    public static void main(String[] args) {
        float preco_compra, preco_venda, impostos;
        float lucro;
        
        Scanner entradas = new Scanner( System.in );
        
        System.out.print("Digite preço de compra do produto: ");
        preco_compra = entradas.nextFloat();
        
        System.out.print("Digite preço de venda do produto: ");
        preco_venda = entradas.nextFloat();

        System.out.print("Digite total de impostos do produto: ");
        impostos = entradas.nextFloat();
     
        lucro = (preco_venda - preco_compra - impostos);
        
        if (lucro > 0)
            System.out.print("Lucro: " + lucro);
        else
            System.out.print("Prejuízo: " + lucro);
        
    }
}


