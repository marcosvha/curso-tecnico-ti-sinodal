public class Dados2G {
//	 Determina o vencedor de um jogo de dados
	public static void main(String[] args) {
		int dA,dB,vA = 0,vB = 0;
		
		IOGraphic.writeln("Jogador A    Jogador B       Vencedor da Jogada");
		do {
			dA = ((int)(Math.random()*1000) % 6)+1;
			dB = ((int)(Math.random()*1000) % 6)+1;
			if (dA != dB) {
				IOGraphic.write("            " + dA + "                     " + dB);
				if (dA > dB) {
					vA++;
					IOGraphic.writeln("                          A");
				}
				else {
					vB++;
					IOGraphic.writeln("                          B");
				}
			}
		} while ((vA!=11) && (vB!=11));
		if (vA==11) IOGraphic.writeln("O vencedor e o jogador: A");
		if (vB==11) IOGraphic.show("O vencedor e o jogador: B");
	}
}
