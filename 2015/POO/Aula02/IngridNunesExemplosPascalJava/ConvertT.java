public class ConvertT {
//	 Converte valores
	public static void main(String[] args) {
		float v;
		int u;
		
		IOText.write("Qual o valor que voce deseja converter? ");
		v = IOText.readFloat();
		IOText.write("Qual a unidade do valor - polegada(1), pe(2), jarda(3), milha(4)? ");
		u = IOText.readInt();
		switch (u) {
		  case 1: IOText.writeln("Valor convertido: " + v*2.54 + "cm");
		          break;
		  case 2: IOText.writeln("Valor convertido: " + v*30.48 + "cm");
		          break;
		  case 3: IOText.writeln("Valor convertido: " + v*0.9144 + "m");
		          break;
		  case 4: IOText.writeln("Valor convertido: " + v*1.609 + "km");
		          break;
		}
	}
}