program Convert;
uses crt;
{Converte valores}
var
 V : real;
 U : integer;
begin
 clrscr;
 write('Qual o valor que voce deseja converter? ');
 readln(V);
 write('Qual a unidade do valor - polegada(1), pe(2), jarda(3), milha(4)? ');
 readln(U);
 case U of
  1 : writeln('Valor convertido: ',V*2.54:8:2,'cm');
  2 : writeln('Valor convertido: ',V*30.48:8:2,'cm');
  3 : writeln('Valor convertido: ',V*0.9144:8:2,'m');
  4 : writeln('Valor convertido: ',V*1.609:8:2,'km');
 end;
 readln;
end.
