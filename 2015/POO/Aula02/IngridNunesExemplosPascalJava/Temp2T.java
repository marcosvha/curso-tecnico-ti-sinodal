public class Temp2T {
//	 Converte de graus F em C
	public static void main(String[] args) {
		float f;
		
		IOText.writeln("Temperatura em F: ");
		f = IOText.readFloat();
		IOText.writeln("Temperatura em C: " + (f - 32) * 5/9); 
	}
}
