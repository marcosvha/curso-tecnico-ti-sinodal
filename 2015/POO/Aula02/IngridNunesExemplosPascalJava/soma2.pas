Program SOMA2;
{Le dois numeros inteiros; imprime os dois numeros lidos e sua soma}
var
  A,B:integer      {Numeros lidos}
  SOMA:integer     {Soma dos numeros lidos}
begin
  write('Valor de A: ');
  readln(A);
  write('Valor de B: ');
  readln(B);
  writeln('Valores lidos: ',A,' ',B);
  SOMA = A + B;
  writeln('Soma: ',SOMA);
end.