public class Dados2T {
//	 Determina o vencedor de um jogo de dados
	public static void main(String[] args) {
		int dA,dB,vA = 0,vB = 0;
		
		IOText.writeln("Jogador A    Jogador B       Vencedor da Jogada");
		do {
			dA = ((int)(Math.random()*1000) % 6)+1;
			dB = ((int)(Math.random()*1000) % 6)+1;
			if (dA != dB) {
				IOText.write("    " + dA + "             " + dB);
				if (dA > dB) {
					vA++;
					IOText.writeln("                 A");
				}
				else {
					vB++;
					IOText.writeln("                 B");
				}
			}
		} while ((vA!=11) && (vB!=11));
		if (vA==11) IOText.writeln("O vencedor e o jogador: A");
		if (vB==11) IOText.writeln("O vencedor e o jogador: B");
	}
}
