public class ConvertG {
//	 Converte valores
	public static void main(String[] args) {
		float v;
		int u;
		
		v = IOGraphic.readFloat("Qual o valor que voce deseja converter? ");
		u = IOGraphic.readInt("Qual a unidade do valor - polegada(1), pe(2), jarda(3), milha(4)? ");
		switch (u) {
		  case 1: IOGraphic.show("Valor convertido: " + v*2.54 + "cm");
		          break;
		  case 2: IOGraphic.show("Valor convertido: " + v*30.48 + "cm");
		          break;
		  case 3: IOGraphic.show("Valor convertido: " + v*0.9144 + "m");
		          break;
		  case 4: IOGraphic.show("Valor convertido: " + v*1.609 + "km");
		          break;
		}
	}
}