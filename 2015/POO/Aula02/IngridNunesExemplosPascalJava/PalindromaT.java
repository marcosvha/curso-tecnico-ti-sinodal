public class PalindromaT {
//	Le palavra e diz se ela e palindroma
	public static boolean palin(String pal) {
		int i;
		boolean palindroma = true;
		
		pal.toUpperCase();
		for (i = 0;i < (pal.length()/2);i++) {
			if (pal.charAt(i)!=pal.charAt(pal.length()-i-1))
				palindroma=false;
		}
		return palindroma;
	}
	public static void main (String[] args) {
		String pal,texto;
		int i = 0;
		
		IOText.writeln("Digite um texto: ");
		texto = IOText.readString();
		do {
			pal="";
			while ((texto.charAt(i)!=' ') && (texto.charAt(i)!='.') && (i!=texto.length())) {
				pal+=texto.charAt(i);
				i++;
			}
			if (pal!="") {
				if (PalindromaT.palin(pal))
					IOText.writeln(pal+" - palindroma");
				else
					IOText.writeln(pal+" - nao e palindroma");
			}
			i++;
		} while (i < texto.length());
	}	
}