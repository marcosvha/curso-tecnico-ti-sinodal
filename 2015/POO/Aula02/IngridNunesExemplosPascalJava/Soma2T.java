public class Soma2T {
//	Le dois numeros inteiros; imprime os dois numeros lidos e sua soma
	public static void main(String[] args) {
		int a,b;      //Numeros Lidos
		int soma;     //Soma dos Numeros Lidos
		
		IOText.write("Valor de A: ");
		a = IOText.readInt();
		IOText.write("Valor de B: ");
		b = IOText.readInt();
		IOText.writeln("Valores lidos: " + a + " " + b);
		soma = a + b;
		IOText.writeln("Soma: " + soma);
	}
}