public class Soma2G {
//	Le dois numeros inteiros; imprime os dois numeros lidos e sua soma
	public static void main(String[] args) {
		int a,b;      //Numeros Lidos
		int soma;     //Soma dos Numeros Lidos
		
		a = IOGraphic.readInt("Valor de A: ");
		b = IOGraphic.readInt("Valor de B: ");
		IOGraphic.writeln("Valores lidos: " + a + " " + b);
		soma = a + b;
		IOGraphic.writeln("Soma: " + soma);
		IOGraphic.show();
	}
}