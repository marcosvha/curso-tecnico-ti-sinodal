program palavra;
{Le palavra e diz se ela e palindroma}
var
  Pal,Texto : string;
  I: integer;
function PALIN (Pal:string): boolean;
  var
    I : integer;
  begin
    Pal:=upcase(Pal);
    PALIN := true;
    for I:=1 to (ord(Pal[0]) div 2) do
      begin
        if (Pal[I]) <> (Pal[ord(Pal[0])+1-I])
          then
            PALIN := false;
      end;
  end;
begin
  writeln('Digite um texto: ');
  readln(Texto);
  I:=0;
  repeat
    Pal:='';
    I:=I+1;
    while (Texto[I]<>' ') and (Texto[I]<>',') and (Texto[I]<>'.') and (I <> ord(Texto[0])+1) do
      begin
        Pal:=Pal+Texto[I];
        I:=I+1;
      end;
    if (Pal <> '')
      then
        if PALIN(Pal)
          then writeln(Pal,' - palindroma')
          else writeln(Pal,' - nao e palindroma');
  until I >= ord(Texto[0]);
  readln;
end.
