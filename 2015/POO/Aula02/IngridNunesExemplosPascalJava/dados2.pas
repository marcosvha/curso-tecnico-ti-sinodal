program dados;
{Determina o vencedor de um jogo de daods}
var
  DA,DB,VA,VB : integer;
begin
  randomize;
  VA := 0;
  VB := 0;
  writeln('Jogador A    Jogador B       Vencedor da Jogada');
  repeat
    DA := Random(6)+1;
    DB := Random(6)+1;
    if DA <> DB
      then
       begin
        write(DA:4,DB:13);
        if DA > DB
          then
            begin
              VA := VA+1;
              writeln('                 A');
            end
          else
            begin
              VB := VB+1;
              writeln('                 B');
            end;
      end;
  until (VA=11) or (VB=11);
  if VA=11 then writeln('O vencedor e o jogador: A');
  if VB=11 then writeln('O vencedor e o jogador: B');
  readln;
end.