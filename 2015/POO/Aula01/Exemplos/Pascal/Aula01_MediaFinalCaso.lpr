{ATENÇÃO: Esta rotina ainda está em desenvolvimento.
          O código gerado pode apresentar incorreções sintáticas. }
program Aula01_MediaFinalCaso;
uses Crt;
{ Função : Demonstra a avaliação de múltiplas expressões para uma mesma opção (caso)}
{ Autor :}
{ Data : 13/06/2014}
{ Seção de Declarações}
var
media_final: real;
begin
   { Seção de Comandos}
   write('Digite a média final do aluno: ');
   readln(media_final)         { Valor digitado do tipo REAL};
{Atenção: As restrições de case ... of no Pascal são maiores que de
          escolha ... fimescolha no Visualg.}
   case Trunc(media_final)   of
   0  ..  4 :
   begin
      write('Aluno reprovado');
   end;
   5,6 :
   begin
      write('Aluno em recuperação');
   end;
   7..10:
   begin
      write('Aluno aprovado!');
   end;
   else
   begin
      write('Nota inválida');
   end;
   end;

   readkey();
end.
