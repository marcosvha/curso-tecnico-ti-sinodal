program calcula_lucro_produto2;
uses Crt;
var
   preco_compra, preco_venda, impostos: real;
   diferenca: real;
begin
   crt.ClrScr; // Limpa a tela

   write('Digite preco de compra do produto: ');
   readln(preco_compra);
   write('Digite preco de venda do produto: ');
   readln(preco_venda);
   write('Digite total de impostos do produto: ');
   readln(impostos);
   diferenca := (preco_venda - preco_compra - impostos);

   if diferenca > 0 then begin
      writeln('Lucro: ', diferenca:2:2);
   end else begin
      writeln('Prejuizo: ', diferenca:2:2);
   end;

   crt.ReadKey(); // Aguarda pressionamento de tecla
end.
